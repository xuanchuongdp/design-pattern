package flyweight;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

public class Forest extends JFrame {
	private static final long serialVersionUID = 7052106620735994538L;

	private List<Tree> trees = new ArrayList<>();

	public void plantTree(int x, int y, String name, Color color,
			String otherTreeData) {
		TreeType treeType = TreeFactory.getTreeType(name, color, otherTreeData);
		Tree tree = new Tree(x, y, treeType);
		trees.add(tree);
	}

	@Override
	public void paint(Graphics g) {
		trees.forEach(tree -> tree.draw(g));
	}
}
