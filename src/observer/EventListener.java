package observer;

/**
 * Here's the subscriber interface.
 * @author nxchuong
 *
 */
public interface EventListener {
	void update(String fileName);
}
