package iterator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Profile {
	private String name;
	private String email;
	private Map<String, List<String>> contacts = new HashMap<>();
	public Profile(String email, String name, String... contacts) {
		this.email = email;
		this.name = name;
		
	}
	public String getName() {
		return name;
	}
	public String getEmail() {
		return email;
	}
	public Map<String, List<String>> getContacts() {
		return contacts;
	}

}
