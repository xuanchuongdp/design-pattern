package decorator;

public class Header2 extends TicketDecorator{

	public Header2(Component myComp) {
		super(myComp);
	}

	public void prtTicket() {
		System.out.println("place printing header 2 code here");
		super.prtTicket();
	}
}
