package decorator;

public class Footer1 extends TicketDecorator{

	public Footer1(Component myComp) {
		super(myComp);
	}

	public void prtTicket() {
		System.out.println("place printing footer 1 code here");
		super.prtTicket();
	}
}
