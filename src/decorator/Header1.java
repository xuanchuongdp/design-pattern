package decorator;

public class Header1 extends TicketDecorator{
	public Header1(Component myComp) {
		super(myComp);
	}
	
	public void prtTicket() {
		System.out.println("place printing header 1 code here");
		super.prtTicket();
	}
}
