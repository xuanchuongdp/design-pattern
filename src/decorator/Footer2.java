package decorator;

public class Footer2 extends TicketDecorator{

	public Footer2(Component myComp) {
		super(myComp);
	}

	@Override
	public void prtTicket() {
		System.out.println("place printing footer 2 code here");
		super.prtTicket();
	}
}
