package decorator;

public final class Configuration {
	public static final Component getSalesTicket() {
		return new Header1(new Footer1(new SalesTicket()));
	}
}
