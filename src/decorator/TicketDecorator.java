package decorator;

public abstract class TicketDecorator extends Component{
	private Component myComp;
	
	public TicketDecorator(Component myComp) {
		this.myComp = myComp;
	}
	
	public void prtTicket() {
		if (myComp != null) {
			myComp.prtTicket();
		}
	}
}
